public class aula54 { // arrays multidimensionais II
    public static void main(String[] args) {

        int [] [] nums = new int [2] [9]; // array 2 dimensões, primeira posição 2 dimensões, segunda posição 9 dimensões
        /*nums[0] [0] = 1000; // posição 0 valor 1000
        System.out.println(nums[0][0]); // imprime msg
        nums[1] [0] = 5; // posição 0 valor 1000
        System.out.println(nums[1][0]); // imprime msg

        System.out.println(nums[0].length);
        */
        nums[0] [0] = 1;
        nums[0] [1] = 2;
        nums[0] [2] = 3;
        nums[0] [3] = 4;
        nums[0] [4] = 5;
        nums[0] [5] = 6;
        nums[0] [6] = 7;
        nums[0] [7] = 8;
        nums[0] [8] = 9;

        for(int x = 0; x<nums[0] .length; x++){// variavel x valor 0 , x menor 0 length , incrementa 1 unidade a x
            System.out.println(nums[0][x]); // imprime , quando x for igual a 1, imprime a primeira posiçao do array
        }

    }
}
